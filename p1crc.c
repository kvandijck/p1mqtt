#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

/* algorithm ported to C from
 * https://stackoverflow.com/questions/41901830/p1-meter-crc16-checksum#41907049
 */

static uint16_t polynomial = 0xa001;
static const uint16_t seed = 0;

static uint16_t table[256];
static int table_filled = 0;

static void p1crc_generate_table(void)
{
	uint16_t value;
	uint16_t temp;
	int i, j;

        for (i = 0; i < 256; ++i) {
		value = 0;
		temp = i;
		for (j = 0; j < 8; ++j)
		{
			if (((value ^ temp) & 0x0001) != 0)
				value = (value >> 1) ^ polynomial;
			else
				value >>= 1;
			temp >>= 1;
		}
		table[i] = value;
        }
	table_filled = 1;
	if (strtol(getenv("SHOW_CRC_TABLE") ?: "", NULL, 0)) {
		for (j = 0; j < 256; ++j) {
			if (j % 8)
				printf(" ");
			printf("0x%04x,", table[j]);
			if (!((j+1)%8))
				printf("\n");
		}
	}
}

int p1crc(const void *vdat, int len)
{
	const uint8_t *dat = vdat;
	int j, idx;
	uint16_t crc = seed;

	if (!table_filled)
		p1crc_generate_table();

        for (j = 0; j < len; ++j) {
            idx = (crc ^ dat[j]) & 0xff;
            crc = (crc >> 8) ^ table[idx];
        }
        return crc;
}
