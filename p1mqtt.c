#include <ctype.h>
#include <errno.h>
#include <signal.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>
#include <syslog.h>
#include <sys/signalfd.h>
#include <sys/uio.h>
#include <termios.h>
#include <mosquitto.h>

#include "lib/libt.h"
#include "lib/libe.h"

/* Should be included from a header file ... */
extern int p1crc(const void *dat, int len);

#define NAME "p1mqtt"
#ifndef VERSION
#define VERSION "<undefined version>"
#endif

/* generic error logging */
#define LOG_MQTT	0x4000000

/* safeguard our LOG_EXIT extension */
#if (LOG_MQTT & (LOG_FACMASK | LOG_PRIMASK))
#error LOG_MQTT conflict
#endif
static void mqttlog(int loglevel, const char *str);

static int max_loglevel = LOG_WARNING;
static int logtostderr = -1;

static void set_loglevel(int new_loglevel)
{
	max_loglevel = new_loglevel;
	if (!logtostderr)
		setlogmask(LOG_UPTO(max_loglevel));
}

__attribute((format(printf,2,3)))
void mylog(int loglevel, const char *fmt, ...)
{
	va_list va;
	char *str;

	if (logtostderr < 0) {
		int fd;

		fd = open("/dev/tty", O_RDONLY | O_NOCTTY);
		if (fd >= 0)
			close(fd);
		/* log to stderr when we're in a terminal */
		logtostderr = fd >= 0;
		if (!logtostderr) {
			openlog(NAME, 0, LOG_LOCAL2);
			setlogmask(LOG_UPTO(max_loglevel));
		}
	}

	/* render string */
	va_start(va, fmt);
	vasprintf(&str, fmt, va);
	va_end(va);

	if (!logtostderr) {
		syslog(loglevel & LOG_PRIMASK, "%s", str);

	} else if ((loglevel & LOG_PRIMASK) <= max_loglevel) {
		struct timespec tv;
		char timbuf[64];

		clock_gettime(CLOCK_REALTIME, &tv);
		strftime(timbuf, sizeof(timbuf), "%b %d %H:%M:%S", localtime(&tv.tv_sec));
		sprintf(timbuf+strlen(timbuf), ".%03u ", (int)(tv.tv_nsec/1000000));

		struct iovec vec[] = {
			{ .iov_base = timbuf, .iov_len = strlen(timbuf), },
			{ .iov_base = NAME, .iov_len = strlen(NAME), },
			{ .iov_base = ": ", .iov_len = 2, },
			{ .iov_base = str, .iov_len = strlen(str), },
			{ .iov_base = "\n", .iov_len = 1, },
		};
		writev(STDERR_FILENO, vec, sizeof(vec)/sizeof(vec[0]));
	}
	if (loglevel & LOG_MQTT) {
		mqttlog(loglevel, str);
	}

	free(str);
	if ((loglevel & LOG_PRIMASK) <= LOG_ERR)
		exit(1);
}
#define ESTR(num)	strerror(num)

/* program options */
static const char help_msg[] =
	NAME ": forward P1 measurements to MQTT\n"
	"usage:	" NAME " [OPTIONS ...] PORT\n"
	"\n"
	"Options\n"
	" -V, --version		Show version\n"
	" -v, --verbose		Be more verbose\n"
	" -m, --mqtt=HOST[:PORT]Specify alternate MQTT host+port\n"
	" -n, --dryrun		No action\n"

	" -w, --work=TOPIC	TOPIC base for working state\n"
	" -r, --invert		Invert direction for aggregated values\n"
	" -a, --all		Publish all values that contribute to aggregated values\n"
	;

#ifdef _GNU_SOURCE
static struct option long_opts[] = {
	{ "help", no_argument, NULL, '?', },
	{ "version", no_argument, NULL, 'V', },
	{ "verbose", no_argument, NULL, 'v', },

	{ "mqtt", required_argument, NULL, 'm', },
	{ "dryrun", required_argument, NULL, 'n', },

	{ "work", required_argument, NULL, 'w', },
	{ "invert", no_argument, NULL, 'r', },
	{ "all", no_argument, NULL, 'a', },
	{ },
};
#else
#define getopt_long(argc, argv, optstring, longopts, longindex) \
	getopt((argc), (argv), (optstring))
#endif
static const char optstring[] = "Vv?m:nw:ra";

/* config */
static const char *work_topic = "p1";
static int work_topiclen;

static int dryrun;
static double reverse = 1;
static int all;
static const char *mqtt_host = "localhost";
static int mqtt_port = 1883;
static int mqtt_keepalive = 0;
static int mqtt_qos = 0;
static int pub_sent_mid, pub_complete_mid;

static const char *p1_port;

/* state */
static struct mosquitto *mosq;

static char *saved_state;
static char *saved_name;

#define xfree(x)	({ if (x) free(x); (x) = NULL; })

static void mqtt_pub(const char *topic, const char *payload, int retain, char **cached);
static const char *payloadfmt(const char *fmt, ...);
static const char *topicfmt(const char *fmt, ...);

static void open_port(void *dat);

/* signal handler */
static volatile int sigterm;

static void signalrecvd(int fd, void *dat)
{
	int ret;
	struct signalfd_siginfo sfdi;

	for (;;) {
		ret = read(fd, &sfdi, sizeof(sfdi));
		if (ret < 0 && errno == EAGAIN)
			break;
		if (ret < 0)
			mylog(LOG_ERR, "read signalfd: %s", ESTR(errno));
		switch (sfdi.ssi_signo) {
		case SIGTERM:
		case SIGINT:
			sigterm = 1;
			break;
		}
	}
}

/* usefull tricks */
static char *csprintf_str;
__attribute__((format(printf,1,2)))
static const char *csprintf(const char *fmt, ...)
{
	va_list va;

	xfree(csprintf_str);
	va_start(va, fmt);
	vasprintf(&csprintf_str, fmt, va);
	va_end(va);
	return csprintf_str;
}

__attribute__((unused))
static double strtod_safe(const char *str)
{
	double val;
	char *endp;

	val = strtod(str, &endp);
	if (endp > str)
		return val;
	return NAN;
}

/* p1 iface */
static char rbuf[16*1024];
static int rfill;

struct element {
	const char *key;
	const char *topic;
	int idx;
	int flags;
		#define FL_DATETIME	(1 << 0)
		#define FL_DAY_NIGHT	(1 << 1)
		#define FL_PUBLISH	(1 << 2)
	union {
		int dec;
		int max_interval;
	};
	struct {
		union {
			/* source */
			struct {
				double factor;
				struct element *el;
			};
			/* sink */
			double sum;
		};
	} aggr;
	time_t recvd;
	union {
		double value;
		time_t tval;
	};
	char *saved;
};

static time_t parse_datetime(const char *str)
{
	unsigned long long val;
	char *end;
	struct tm tm = {};

	/* decode BCD-like */
	val = strtoull(str, &end, 10);
	tm.tm_isdst = *end != 'W';
	tm.tm_sec = val % 100;
	val /= 100;
	tm.tm_min = val % 100;
	val /= 100;
	tm.tm_hour = val % 100;
	val /= 100;
	tm.tm_mday = val % 100;
	val /= 100;
	tm.tm_mon = (val % 100)-1;
	val /= 100;
	/* tm_year starts from 1900, input starts from 2000 */
	tm.tm_year = (val % 100)+100;

	/* normalize */
	return mktime(&tm);
}

static const char *conv_enum(const char *str, const char *const*table, int tablesize)
{
	int idx = strtol(str, NULL, 0);
	const char *result;

	if (idx >= tablesize)
		result = NULL;
	else
		result = table[idx];
	return result ?: str;
}

static const char *const day_night[] = {
	[1] = "d",
	[2] = "n",
};
#define ARRAY_SIZE(x) (sizeof(x)/sizeof((x)[0]))

static struct element elements[] = {
	{ .topic = "P", .dec = 3, },
#define AGG_P (elements+0)
	{ .topic = "P/1", .dec = 3, },
#define AGG_P_1 (elements+1)
	{ .topic = "P/2", .dec = 3, },
#define AGG_P_2 (elements+2)
	{ .topic = "P/3", .dec = 3, },
#define AGG_P_3 (elements+3)
	{ .topic = "E/cons", .dec = 3, },
#define AGG_E_CONS (elements+4)
	{ .topic = "E/prod", .dec = 3, },
#define AGG_E_PROD (elements+5)

	{ "1-0:1.8.1", "E/d/cons", .flags = FL_PUBLISH, .dec = 3, .aggr.el = AGG_E_CONS, .aggr.factor = 1, },
	{ "1-0:1.8.2", "E/n/cons", .flags = FL_PUBLISH, .dec = 3, .aggr.el = AGG_E_CONS, .aggr.factor = 1, },
	{ "1-0:2.8.1", "E/d/prod", .flags = FL_PUBLISH, .dec = 3, .aggr.el = AGG_E_PROD, .aggr.factor = 1, },
	{ "1-0:2.8.2", "E/n/prod", .flags = FL_PUBLISH, .dec = 3, .aggr.el = AGG_E_PROD, .aggr.factor = 1, },
	{ "1-0:32.7.0", "V/1", },
	{ "1-0:52.7.0", "V/2", },
	{ "1-0:72.7.0", "V/3", },
	{ "1-0:31.7.0", "A/1", .dec = 3, },
	{ "1-0:51.7.0", "A/2", .dec = 3, },
	{ "1-0:71.7.0", "A/3", .dec = 3, },
	{ "1-0:1.7.0", "P/cons", .dec = 3, .aggr.el = AGG_P, .aggr.factor = 1, },
	{ "1-0:2.7.0", "P/prod", .dec = 3, .aggr.el = AGG_P, .aggr.factor = -1, },
	{ "1-0:21.7.0", "P/1/cons", .dec = 3, .aggr.el = AGG_P_1, .aggr.factor = 1, },
	{ "1-0:41.7.0", "P/2/cons", .dec = 3, .aggr.el = AGG_P_2, .aggr.factor = 1, },
	{ "1-0:61.7.0", "P/3/cons", .dec = 3, .aggr.el = AGG_P_3, .aggr.factor = 1, },
	{ "1-0:22.7.0", "P/1/prod", .dec = 3, .aggr.el = AGG_P_1, .aggr.factor = -1, },
	{ "1-0:42.7.0", "P/2/prod", .dec = 3, .aggr.el = AGG_P_2, .aggr.factor = -1, },
	{ "1-0:62.7.0", "P/3/prod", .dec = 3, .aggr.el = AGG_P_3, .aggr.factor = -1, },

	{ "0-0:96.1.4", "info/id", },
	{ "0-0:96.13.0", "info/msg", },
	{ "0-0:96.14.0", "e/rate", .flags = FL_DAY_NIGHT, },
	{ "0-0:1.0.0", "e/time", .flags = FL_DATETIME, .max_interval = 2, },
	{ "0-0:96.1.1", "e/serial", },
	{ "0-1:96.1.1", "g/serial", },
	{ "0-0:96.3.10", "e/switch", },
	{ "0-1:24.4.0", "g/switch", },

	{ "0-1:24.2.3", "g/time", .flags = FL_DATETIME, .max_interval = 300, },
	{ "0-1:24.2.3", "g/V", .idx = 1, .dec = 3, },
	{},
};

static struct element *lookup(const char *key, int idx)
{
	struct element *table = elements;

	for (; table->topic; ++table) {
		if (!table->key)
			continue;
		if (!strcmp(key, table->key) && idx == table->idx)
			return table;
	}
	return NULL;
}

static int silent_offline;
static void verbose_offline_again(void *dat)
{
	silent_offline = 0;
}

static void p1_offline(void *dat)
{
	if (!silent_offline) {
		mylog(LOG_WARNING | LOG_MQTT, "%s: no data", p1_port ?: "-");
		libt_add_timeout(120, verbose_offline_again, NULL);
		silent_offline = 1;
	}
	mqtt_pub(topicfmt("%s/state", work_topic), "offline", 1, &saved_state);
	int j;
	for (j = 0; elements[j].topic; ++j) {
		mqtt_pub(topicfmt("%s/%s", work_topic, elements[j].topic), "", 1, &elements[j].saved);
		elements[j].recvd = 0;
	}

	if (!p1_port) {
		sigterm = 1;
		return;
	}
	int fd = (long)dat;

	libe_remove_fd(fd);
	close(fd);
	libt_add_timeout(10, open_port, NULL);
}

__attribute__((unused))
static void p1_recvd_payload(char *str, int len, int fd)
{
	char *tok, *saved_tok;
	char *key, *val, *saved_val;
	const char *vstr;
	double dval;
	struct element *el;
	time_t t;
	int idx;
	time_t now;

	time(&now);
	libt_add_timeout(5, p1_offline, (void *)(long)fd);
	mqtt_pub(topicfmt("%s/state", work_topic), "online", 1, &saved_state);
	mylog(LOG_DEBUG, "p1:< %s", str);
	/* reset aggregated topics */
	for (el = elements; el->topic; ++el) {
		if (el->key)
			continue;
		el->aggr.sum = 0;
	}
	/* parse read topics */
	for (tok = strtok_r(str, "\r\n", &saved_tok); tok; tok = strtok_r(NULL, "\r\n", &saved_tok)) {
		if (*tok == '/') {
			mqtt_pub(topicfmt("%s/info/name", work_topic), tok+1, 1, &saved_name);
			continue;
		}
		key = strtok_r(tok, "()", &saved_val);
		if (!key)
			continue;
		for (idx = 0;; ++idx) {
			val = strtok_r(NULL, "()", &saved_val);
			if (!key || !val)
				break;
			el = lookup(key, idx);
			if (!el)
				continue;
			if (el->flags & FL_DATETIME) {
				t = parse_datetime(val);
				vstr = payloadfmt("%llu", (unsigned long long)t);
				if (el->max_interval && el->recvd && ((t < el->tval) || ((t - el->tval - (now - el->recvd)) > el->max_interval*5/2))) {
					mylog(LOG_WARNING | LOG_MQTT, "time jump %s/%s: %lu -> %lu",
							work_topic, el->topic, (long)el->tval, (long)t);
				}
				el->tval = t;
			} else if (el->flags & FL_DAY_NIGHT) {
				vstr = conv_enum(val, day_night, ARRAY_SIZE(day_night));
			} else {
				dval = strtod(val, NULL);
				vstr = payloadfmt("%.*f", el->dec, dval);
				if (el->aggr.el) {
					el->aggr.el->aggr.sum += el->aggr.factor * dval * reverse;
					el->aggr.el->recvd = now;
					if (!all && !(el->flags & FL_PUBLISH))
						/* no more publish here */
						continue;
				}
			}
			el->recvd = now;
			mqtt_pub(topicfmt("%s/%s", work_topic, el->topic), vstr, 1, &el->saved);
		}
	}
	/* publish aggregated topics */
	for (el = elements; el->topic; ++el) {
		if (el->key)
			/* not an aggregated value */
			continue;
		if (el->recvd < now)
			/* not recent */
			continue;
		/* publish aggregated topic */
		vstr = payloadfmt("%.*f", el->dec, el->aggr.sum);
		mqtt_pub(topicfmt("%s/%s", work_topic, el->topic), vstr, 1, &el->saved);
	}
	/* empty lost topics */
	for (el = elements; el->topic; ++el) {
		if (!el->recvd)
			continue;
		if ((now - el->recvd) > 5) {
			mqtt_pub(topicfmt("%s/%s", work_topic, el->topic), "", 1, &el->saved);
			if (el->key)
				mylog(LOG_WARNING | LOG_MQTT, "lost %s/%s", work_topic, el->topic);
			/* mark as never received */
			el->recvd = 0;
		}
	}
}

static void p1_data_recvd(int fd, void *dat)
{
	int ret;
	char *buf;

	ret = read(fd, rbuf+rfill, sizeof(rbuf)-1-rfill);
	if (ret < 0 && errno == EAGAIN)
		return;
	if (ret < 0)
		mylog(LOG_ERR, "recv %s: %s", p1_port ?: "-", ESTR(errno));
	if (!ret) {
		mylog(LOG_ERR, "eof %s", p1_port ?: "-");
		close(fd);
		libe_remove_fd(fd);
		sigterm = 1;
		return;
	}
	rfill += ret;
	/* parse */
	rbuf[rfill] = 0;
	buf = rbuf;

	char *pkt, *crc;
	int rcons = 0;
	for (;;) {
		pkt = strchr(buf+rcons, '/');
		if (!pkt) {
			/* discard all */
			rfill = 0;
			return;
		}
		crc = strchr(pkt, '!');
		if (!crc)
			/* wait for crc */
			return;
		if ((crc+6) > rbuf+rfill)
			/* wait for more bytes */
			return;

		uint16_t mycrc = p1crc((void *)pkt, (crc+1)-(pkt));
		uint16_t pkcrc = strtoul(crc+1, NULL, 16);

		rcons = crc+6-buf;
		if (pkcrc != mycrc) {
			mylog(LOG_WARNING, "crc wrong %04x~%04x", mycrc, pkcrc);
		} else {
			*crc = 0;
			p1_recvd_payload(pkt, crc-pkt, fd);
		}
	}
	if (rcons < rfill) {
		memmove(buf, buf+rcons, rfill-rcons);
		rfill -= rcons;
	} else {
		rfill = 0;
	}
}

static void open_port(void *dat)
{
	const char *filename = p1_port;
	int fd;
	struct termios term;

	if (!p1_port)
		return;
	/* open file */
	fd = open(filename, O_RDWR | O_NOCTTY | O_NONBLOCK);
	if (fd < 0)
		mylog(LOG_ERR, "open %s: %s", filename, ESTR(errno));

	/* prepare port */
	if (tcgetattr(fd, &term) < 0) {
		if (errno != ENOTTY)
			mylog(LOG_ERR, "tcgetattr %s: %s", filename, ESTR(errno));
	} else {
		term.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | IXON | INLCR | IGNCR | ICRNL | INPCK);
		term.c_oflag &= ~(OPOST);
		term.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
		term.c_cflag &= ~(CSIZE | PARENB | CSTOPB | PARODD | CRTSCTS | CLOCAL);
		term.c_cflag |= CS8 | HUPCL | CREAD;
		cfsetispeed(&term, B115200);
		cfsetospeed(&term, B115200);

		if (tcsetattr(fd, TCSANOW, &term) < 0)
			mylog(LOG_ERR, "tcsetattr %s: %s", filename, ESTR(errno));
	}

	/* listen to events */
	libe_add_fd(fd, p1_data_recvd, NULL);
	/* set timeout */
	libt_add_timeout(5, p1_offline, (void *)(long)fd);
}

/* mqtt iface */
static char *topicfmt_str;
__attribute__((format(printf,1,2)))
static const char *topicfmt(const char *fmt, ...)
{
	va_list va;

	xfree(topicfmt_str);
	va_start(va, fmt);
	vasprintf(&topicfmt_str, fmt, va);
	va_end(va);
	return topicfmt_str;
}
static char *payloadfmt_str;
__attribute__((unused))
__attribute__((format(printf,1,2)))
static const char *payloadfmt(const char *fmt, ...)
{
	va_list va;

	xfree(payloadfmt_str);
	va_start(va, fmt);
	vasprintf(&payloadfmt_str, fmt, va);
	va_end(va);
	return payloadfmt_str;
}

static void mqtt_pub(const char *topic, const char *payload, int retain, char **cached)
{
	int ret;

	if (cached && retain) {
		if (!strcmp(payload ?: "", *cached ?: ""))
			/* equal */
			return;
	}
	mylog(LOG_INFO, "mqtt:>%s %s", topic, payload ?: "<null>");
	ret = mosquitto_publish(mosq, &pub_sent_mid, topic, strlen(payload?:""), payload, mqtt_qos, retain);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_publish %s: %s", topic, mosquitto_strerror(ret));
	if (cached && retain) {
		xfree(*cached);
		*cached = strdup(payload);
	}
}

static void my_mqtt_pub(struct mosquitto *mosq, void *dat, int mid)
{
	pub_complete_mid = mid;
}

static inline int mqtt_pending(void)
{
	return pub_sent_mid != pub_complete_mid;
}

static void mqtt_sub(const char *topic)
{
	int ret;

	ret = mosquitto_subscribe(mosq, NULL, topic, mqtt_qos);
	if (ret < 0)
		mylog(LOG_ERR, "mosquitto_subscribe %s: %s", topic, mosquitto_strerror(ret));
}

static void mqttlog(int loglevel, const char *msg)
{
	static const char *const prionames[] = {
		[LOG_EMERG] = "emerg",
		[LOG_ALERT] = "alert",
		[LOG_CRIT] = "crit",
		[LOG_ERR] = "err",
		[LOG_WARNING] = "warn",
		[LOG_NOTICE] = "notice",
		[LOG_INFO] = "info",
		[LOG_DEBUG] = "debug",
	};

	mqtt_pub(topicfmt("log/%s/%s", NAME, prionames[loglevel & LOG_PRIMASK]), msg, 0, NULL);
}

static void my_mqtt_log(struct mosquitto *mosq, void *userdata, int level, const char *str)
{
	static const int logpri_map[] = {
		MOSQ_LOG_ERR, LOG_ERR,
		MOSQ_LOG_WARNING, LOG_WARNING,
		MOSQ_LOG_NOTICE, LOG_NOTICE,
		MOSQ_LOG_INFO, LOG_INFO,
		MOSQ_LOG_DEBUG, LOG_DEBUG,
		0,
	};
	int j;

	for (j = 0; logpri_map[j]; j += 2) {
		if (level & logpri_map[j]) {
			//mylog(logpri_map[j+1], "[mosquitto] %s", str);
			return;
		}
	}
}

static int test_set_topic(const char *topic, const char *main, int retain)
{
	if (retain)
		return !strcmp(topic, main);
	int len = strlen(main);

	return strlen(topic) == len+4
		&& !strncmp(topic, main, len)
		&& !strcmp(topic+len, "/set");
}

static void my_mqtt_msg(struct mosquitto *mosq, void *dat, const struct mosquitto_message *msg)
{
	char *topic, *payload;

	mylog(LOG_DEBUG, "mqtt:<%s %s", msg->topic, (char *)msg->payload ?: "<null>");

	if (!strncmp(work_topic, msg->topic, work_topiclen)) {
		topic = msg->topic + work_topiclen;
		payload = (char *)msg->payload;

		if (test_set_topic(topic, "/cfg/loglevel", msg->retain)) {
			if (!msg->payloadlen)
				return;
			set_loglevel(strtoul(payload, NULL, 0));
			mqtt_pub(topicfmt(work_topic, "/cfg/loglevel"), payloadfmt("%i", max_loglevel), 1, NULL);
		}
	}
}

static void mqtt_maintenance(void *dat)
{
	int ret;
	struct mosquitto *mosq = dat;

	ret = mosquitto_loop_misc(mosq);
	if (ret)
		mylog(LOG_ERR, "mosquitto_loop_misc: %s", mosquitto_strerror(ret));
	libt_add_timeout(2.3, mqtt_maintenance, dat);
}

static void recvd_mosq(int fd, void *dat)
{
	struct mosquitto *mosq = dat;
	int evs = libe_fd_evs(fd);
	int ret;

	if (evs & LIBE_RD) {
		/* mqtt read ... */
		ret = mosquitto_loop_read(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_read: %s", mosquitto_strerror(ret));
	}
	if (evs & LIBE_WR) {
		/* flush mqtt write queue _after_ the timers have run */
		ret = mosquitto_loop_write(mosq, 1);
		if (ret)
			mylog(LOG_ERR, "mosquitto_loop_write: %s", mosquitto_strerror(ret));
	}
}

void mosq_update_flags(void)
{
	if (mosq)
		libe_mod_fd(mosquitto_socket(mosq), LIBE_RD | (mosquitto_want_write(mosq) ? LIBE_WR : 0));
}

int main(int argc, char *argv[])
{
	int opt, ret;
	char *str;

	/* argument parsing */
	while ((opt = getopt_long(argc, argv, optstring, long_opts, NULL)) >= 0)
	switch (opt) {
	case 'V':
		fprintf(stderr, "%s %s\nCompiled on %s %s\n",
				NAME, VERSION, __DATE__, __TIME__);
		exit(0);
	case '?':
		fputs(help_msg, stderr);
		exit(0);
	default:
		fprintf(stderr, "unknown option '%c'", opt);
		fputs(help_msg, stderr);
		exit(1);
		break;
	case 'v':
		++max_loglevel;
		break;
	case 'm':
		mqtt_host = optarg;
		str = strrchr(optarg, ':');
		if (str > mqtt_host && *(str-1) != ']') {
			/* TCP port provided */
			*str = 0;
			mqtt_port = strtoul(str+1, NULL, 10);
		}
		break;
	case 'n':
		dryrun = 1;
		break;
	case 'r':
		reverse = -1;
		break;
	case 'a':
		all = 1;
		break;

	case 'w':
		work_topic = optarg;
		break;
	}

	if (optind < argc) {
		/* extra file|device argument */
		p1_port = argv[optind++];
		open_port(NULL);
	} else {
		libe_add_fd(STDIN_FILENO, p1_data_recvd, NULL);
		libt_add_timeout(5, p1_offline, (void *)(long)STDIN_FILENO);
	}

	/* MQTT start */
	work_topiclen = strlen(work_topic);
	mosquitto_lib_init();
	mosq = mosquitto_new(csprintf(NAME "-%i", getpid()), true, 0);
	if (!mosq)
		mylog(LOG_ERR, "mosquitto_new failed: %s", ESTR(errno));

	mosquitto_log_callback_set(mosq, my_mqtt_log);
	mosquitto_message_callback_set(mosq, my_mqtt_msg);
	mosquitto_publish_callback_set(mosq, my_mqtt_pub);
	mosquitto_will_set(mosq, topicfmt("%s/state", work_topic), 4, "lost", mqtt_qos, 1);

	ret = mosquitto_connect(mosq, mqtt_host, mqtt_port, mqtt_keepalive ?: 60);
	if (ret)
		mylog(LOG_ERR, "mosquitto_connect %s:%i: %s", mqtt_host, mqtt_port, mosquitto_strerror(ret));

	/* subscribe to topics */
	mqtt_sub(topicfmt("%s/#", work_topic));

	libt_add_timeout(0, mqtt_maintenance, mosq);
	libe_add_fd(mosquitto_socket(mosq), recvd_mosq, mosq);

	/* prepare signalfd */
	sigset_t sigmask;
	int sigfd;

	sigfillset(&sigmask);
	/* for inside GDB */
	//sigdelset(&sigmask, SIGINT);

	if (sigprocmask(SIG_BLOCK, &sigmask, NULL) < 0)
		mylog(LOG_ERR, "sigprocmask: %s", ESTR(errno));
	sigfd = signalfd(-1, &sigmask, SFD_NONBLOCK | SFD_CLOEXEC);
	if (sigfd < 0)
		mylog(LOG_ERR, "signalfd failed: %s", ESTR(errno));
	libe_add_fd(sigfd, signalrecvd, NULL);

	/* core loop */
	for (; !sigterm;) {
		libt_flush();
		mosq_update_flags();
		libe_poll(libt_get_waittime());
	}
	mylog(LOG_NOTICE, "terminating ...");
	if (!mqtt_qos)
		/* set qos >= 1, so that all is flushed */
		mqtt_qos = 1;

	int j;
	for (j = 0; elements[j].topic; ++j) {
		mqtt_pub(topicfmt("%s/%s", work_topic, elements[j].topic), "", 1, &elements[j].saved);
	}
	mqtt_pub(topicfmt("%s/name", work_topic), "", 1, &saved_name);
	mqtt_pub(topicfmt("%s/state", work_topic), "stopped", 1, &saved_state);

	/* terminate */
	for (; mqtt_pending(); ) {
		libt_flush();
		mosq_update_flags();
		libe_poll(libt_get_waittime());
	}
#if 1
	xfree(payloadfmt_str);
	xfree(topicfmt_str);
	xfree(csprintf_str);

	/* cleanup */
	mosquitto_disconnect(mosq);
	mosquitto_destroy(mosq);
	mosquitto_lib_cleanup();
#endif
	return 0;
}
